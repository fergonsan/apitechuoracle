package com.techu.apitechuoracle.controller;

import com.techu.apitechuoracle.models.ProductModel;
import com.techu.apitechuoracle.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechuoracle")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(this.productService.findAll(),HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public  ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("id es "+id);
        Optional<ProductModel> result = this.productService.findById(id);
//        if (result.isPresent()){
//           return new ResponseEntity<>(result.get(), HttpStatus.OK);
//        }else{
//            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
//        }
        return new ResponseEntity<>(result.isPresent() ? result.get() : "Producto no encontrado", result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel>addProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La ID del producto que se va a crear es "+newProduct.getId());
        System.out.println("La descripcion del producto que se va a crear es "+newProduct.getDesc());
        System.out.println("El precio del producto que se va a crear es "+newProduct.getPrice());
        return new ResponseEntity<>(this.productService.add(newProduct), HttpStatus.CREATED);
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@PathVariable String id, @RequestBody ProductModel product){
        System.out.println("updateProduct");
        System.out.println("La id recibida por parametro es: " + id);
        System.out.println("La id del producto a atualizar es: " + product.getId());
        System.out.println("La descripción del producto a atualizar es: " + product.getDesc());
        System.out.println("El precio del producto a atualizar es: " + product.getPrice());
        ProductModel result = this.productService.update(product);
        return new ResponseEntity<ProductModel>(result, result!=null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct (@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id recibida por parametro es: " + id);
        boolean deletedProduct = this.productService.delete(id);
        return new ResponseEntity<>(deletedProduct ? "El producto se ha borrado" : "Producto no encontrado", deletedProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PatchMapping("/products/{id}")
    public ResponseEntity<Object> patchProduct (@PathVariable String id, @RequestBody ProductModel product){
        System.out.println("patchProduct");
        System.out.println("La id recibida por parametro es: " + id);
        System.out.println("La descripción del producto a actualizar es: " + product.getDesc());
        System.out.println("El precio del producto a actualizar es: " + product.getPrice());
        ProductModel result = this.productService.partialUpdate(product);
        return new ResponseEntity<>(result!=null ? "Producto Actualizado correctamente" : "Producto no encontrado", result!=null ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
