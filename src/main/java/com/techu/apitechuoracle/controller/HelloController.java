package com.techu.apitechuoracle.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public ResponseEntity<String> index(){
        return new ResponseEntity<>("Hola Mundo desde API Tech U!", HttpStatus.OK);
    }

    @RequestMapping("/hello")
    public String hello(@RequestParam(value="name", defaultValue = "Tech U") String name){
        return String.format("Hola %s!", name);
    }
}

