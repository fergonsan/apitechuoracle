package com.techu.apitechuoracle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApitechuoracleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApitechuoracleApplication.class, args);
	}

}
