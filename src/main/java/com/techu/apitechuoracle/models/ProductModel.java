package com.techu.apitechuoracle.models;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class ProductModel {

    private String id;
    private String description;
    private float price;

    public ProductModel() {

    }
    public ProductModel(String id, String desc, float price) {
        this.id = id;
        this.description = desc;
        this.price = price;
    }

    @Id
    @Column(name="ID", nullable = false, length = 255)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
    @Column(name = "DESCRIPTION", nullable = true, length = 255)
    public String getDesc() {
        return this.description;
    }

    public void setDesc(String desc) {
        this.description = desc;
    }
    @Column(name = "PRICE", nullable = true, length = 10)
    public float getPrice() {
        return this.price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
